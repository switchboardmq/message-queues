package clients

import (
	"context"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/service/sns"
)

type ISnsClient interface {
	Publish(ctx context.Context, input *sns.PublishInput) (*sns.PublishOutput, error)
	PublishBatch(ctx context.Context, input *sns.PublishBatchInput) (*sns.PublishBatchOutput, error)
	Subscribe(ctx context.Context, input *sns.SubscribeInput) (*sns.SubscribeOutput, error)
	Unsubscribe(ctx context.Context, input *sns.UnsubscribeInput) (*sns.UnsubscribeOutput, error)
}

type SnsClient struct {
	client *sns.Client
}

func NewSnsClient(cfg aws.Config) ISnsClient {
	return &SnsClient{client: sns.NewFromConfig(cfg)}
}

func (c *SnsClient) Publish(ctx context.Context, input *sns.PublishInput) (*sns.PublishOutput, error) {
	return c.client.Publish(ctx, input)
}

func (c *SnsClient) PublishBatch(ctx context.Context, input *sns.PublishBatchInput) (*sns.PublishBatchOutput, error) {
	return c.client.PublishBatch(ctx, input)
}

func (c *SnsClient) Subscribe(ctx context.Context, input *sns.SubscribeInput) (*sns.SubscribeOutput, error) {
	return c.client.Subscribe(ctx, input)
}

func (c *SnsClient) Unsubscribe(ctx context.Context, input *sns.UnsubscribeInput) (*sns.UnsubscribeOutput, error) {
	return c.client.Unsubscribe(ctx, input)
}
