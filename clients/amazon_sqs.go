package clients

import (
	"context"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/service/sqs"
)

type ISqsClient interface {
	SendMessage(context.Context, *sqs.SendMessageInput) (*sqs.SendMessageOutput, error)
	SendMessageBatch(context.Context, *sqs.SendMessageBatchInput) (*sqs.SendMessageBatchOutput, error)
	DeleteMessage(context.Context, *sqs.DeleteMessageInput) (*sqs.DeleteMessageOutput, error)
	ReceiveMessage(context.Context, *sqs.ReceiveMessageInput) (*sqs.ReceiveMessageOutput, error)
	CreateQueue(ctx context.Context, input *sqs.CreateQueueInput) (*sqs.CreateQueueOutput, error)
	GetQueueAttributes(ctx context.Context, input *sqs.GetQueueAttributesInput) (*sqs.GetQueueAttributesOutput, error)
	DeleteQueue(ctx context.Context, input *sqs.DeleteQueueInput) (*sqs.DeleteQueueOutput, error)
}

type SqsClient struct {
	client *sqs.Client
}

func NewSqsClient(cfg aws.Config) ISqsClient {
	return &SqsClient{client: sqs.NewFromConfig(cfg)}
}

func (s *SqsClient) SendMessage(ctx context.Context, input *sqs.SendMessageInput) (*sqs.SendMessageOutput, error) {
	return s.client.SendMessage(ctx, input)
}

func (s *SqsClient) SendMessageBatch(ctx context.Context, input *sqs.SendMessageBatchInput) (*sqs.SendMessageBatchOutput, error) {
	return s.client.SendMessageBatch(ctx, input)
}

func (s *SqsClient) DeleteMessage(ctx context.Context, input *sqs.DeleteMessageInput) (*sqs.DeleteMessageOutput, error) {
	return s.client.DeleteMessage(ctx, input)
}

func (s *SqsClient) ReceiveMessage(ctx context.Context, input *sqs.ReceiveMessageInput) (*sqs.ReceiveMessageOutput, error) {
	return s.client.ReceiveMessage(ctx, input)
}

func (s *SqsClient) CreateQueue(ctx context.Context, input *sqs.CreateQueueInput) (*sqs.CreateQueueOutput, error) {
	return s.client.CreateQueue(ctx, input)
}

func (s *SqsClient) GetQueueAttributes(ctx context.Context, input *sqs.GetQueueAttributesInput) (*sqs.GetQueueAttributesOutput, error) {
	return s.client.GetQueueAttributes(ctx, input)
}

func (s *SqsClient) DeleteQueue(ctx context.Context, input *sqs.DeleteQueueInput) (*sqs.DeleteQueueOutput, error) {
	return s.client.DeleteQueue(ctx, input)
}
