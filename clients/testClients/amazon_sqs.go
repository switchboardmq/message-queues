package testClients

import (
	"context"
	"github.com/aws/aws-sdk-go-v2/service/sqs"
)

type TestSqsClient struct {
	SendMessageFunc        func(ctx context.Context, input *sqs.SendMessageInput) (*sqs.SendMessageOutput, error)
	SendMessageBatchFunc   func(ctx context.Context, input *sqs.SendMessageBatchInput) (*sqs.SendMessageBatchOutput, error)
	DeleteMessageFunc      func(ctx context.Context, input *sqs.DeleteMessageInput) (*sqs.DeleteMessageOutput, error)
	ReceiveMessageFunc     func(ctx context.Context, input *sqs.ReceiveMessageInput) (*sqs.ReceiveMessageOutput, error)
	CreateQueueFunc        func(ctx context.Context, input *sqs.CreateQueueInput) (*sqs.CreateQueueOutput, error)
	GetQueueAttributesFunc func(ctx context.Context, input *sqs.GetQueueAttributesInput) (*sqs.GetQueueAttributesOutput, error)
	DeleteQueueFunc        func(ctx context.Context, input *sqs.DeleteQueueInput) (*sqs.DeleteQueueOutput, error)
}

func (c *TestSqsClient) SendMessage(ctx context.Context, input *sqs.SendMessageInput) (*sqs.SendMessageOutput, error) {
	return c.SendMessageFunc(ctx, input)
}

func (c *TestSqsClient) SendMessageBatch(ctx context.Context, input *sqs.SendMessageBatchInput) (*sqs.SendMessageBatchOutput, error) {
	return c.SendMessageBatchFunc(ctx, input)
}

func (c *TestSqsClient) DeleteMessage(ctx context.Context, input *sqs.DeleteMessageInput) (*sqs.DeleteMessageOutput, error) {
	return c.DeleteMessageFunc(ctx, input)
}

func (c *TestSqsClient) ReceiveMessage(ctx context.Context, input *sqs.ReceiveMessageInput) (*sqs.ReceiveMessageOutput, error) {
	return c.ReceiveMessageFunc(ctx, input)
}

func (c *TestSqsClient) CreateQueue(ctx context.Context, input *sqs.CreateQueueInput) (*sqs.CreateQueueOutput, error) {
	return c.CreateQueueFunc(ctx, input)
}

func (c *TestSqsClient) GetQueueAttributes(ctx context.Context, input *sqs.GetQueueAttributesInput) (*sqs.GetQueueAttributesOutput, error) {
	return c.GetQueueAttributesFunc(ctx, input)
}

func (c *TestSqsClient) DeleteQueue(ctx context.Context, input *sqs.DeleteQueueInput) (*sqs.DeleteQueueOutput, error) {
	return c.DeleteQueueFunc(ctx, input)
}
