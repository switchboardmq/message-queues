package testClients

import (
	"context"
	"github.com/aws/aws-sdk-go-v2/service/sns"
)

type TestSnsClient struct {
	PublishFunc      func(ctx context.Context, input *sns.PublishInput) (*sns.PublishOutput, error)
	PublishBatchFunc func(ctx context.Context, input *sns.PublishBatchInput) (*sns.PublishBatchOutput, error)
	SubscribeFunc    func(ctx context.Context, input *sns.SubscribeInput) (*sns.SubscribeOutput, error)
	UnsubscribeFunc  func(ctx context.Context, input *sns.UnsubscribeInput) (*sns.UnsubscribeOutput, error)
}

func (c *TestSnsClient) Publish(ctx context.Context, input *sns.PublishInput) (*sns.PublishOutput, error) {
	return c.PublishFunc(ctx, input)
}

func (c *TestSnsClient) PublishBatch(ctx context.Context, input *sns.PublishBatchInput) (*sns.PublishBatchOutput, error) {
	return c.PublishBatchFunc(ctx, input)
}

func (c *TestSnsClient) Subscribe(ctx context.Context, input *sns.SubscribeInput) (*sns.SubscribeOutput, error) {
	return c.SubscribeFunc(ctx, input)
}

func (c *TestSnsClient) Unsubscribe(ctx context.Context, input *sns.UnsubscribeInput) (*sns.UnsubscribeOutput, error) {
	return c.UnsubscribeFunc(ctx, input)
}
