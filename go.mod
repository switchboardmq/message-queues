module gitlab.com/switchboardmq/messagequeues

go 1.19

require (
	github.com/aws/aws-sdk-go-v2 v1.16.15
	github.com/aws/aws-sdk-go-v2/config v1.17.6
	github.com/aws/aws-sdk-go-v2/service/iam v1.18.18
	github.com/aws/aws-sdk-go-v2/service/sns v1.18.0
	github.com/aws/aws-sdk-go-v2/service/sqs v1.19.9
	github.com/google/uuid v1.3.0
	gitlab.com/switchboardmq/publicmodels v0.0.2
	go.uber.org/multierr v1.8.0
)

require (
	github.com/aws/aws-sdk-go-v2/credentials v1.12.19 // indirect
	github.com/aws/aws-sdk-go-v2/feature/ec2/imds v1.12.16 // indirect
	github.com/aws/aws-sdk-go-v2/internal/configsources v1.1.22 // indirect
	github.com/aws/aws-sdk-go-v2/internal/endpoints/v2 v2.4.16 // indirect
	github.com/aws/aws-sdk-go-v2/internal/ini v1.3.23 // indirect
	github.com/aws/aws-sdk-go-v2/service/internal/presigned-url v1.9.16 // indirect
	github.com/aws/aws-sdk-go-v2/service/sso v1.11.22 // indirect
	github.com/aws/aws-sdk-go-v2/service/ssooidc v1.13.4 // indirect
	github.com/aws/aws-sdk-go-v2/service/sts v1.16.18 // indirect
	github.com/aws/smithy-go v1.13.3 // indirect
	go.uber.org/atomic v1.7.0 // indirect
)
