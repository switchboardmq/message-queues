package messagequeues

import (
	"github.com/google/uuid"
	"gitlab.com/switchboardmq/publicmodels"
	"strconv"
	"sync"
)

const (
	PROVIDER_SQS = "SQS"
)

type IMessageService interface {
	Initialize() error
	SubscribeToQueue(queueName string, bufferSize int) (<-chan *publicmodels.QueueMessage, error)
	SubscribeToTopic(topicName string, filter map[string][]any, bufferSize int) (<-chan *publicmodels.QueueMessage, error)
	UnsubscribeFromQueue(queueName string) error
	UnsubscribeFromTopic(topicName string) error
	PublishSingleMessageToQueue(queueName string, message any) error
	PublishBulkMessagesToQueue(queueName string, messages any) error
	PublishSingleMessageToTopic(topicName string, message any) error
	PublishBulkMessagesToTopic(topicName string, messages any) error
	registerSubscription(subscription *ActiveSubscription) error
	unregisterSubscription(subscription ActiveSubscription) error
}

func NewMessageService(providerName string, wg *sync.WaitGroup, config map[string]string) IMessageService {
	switch providerName {
	case PROVIDER_SQS:
		if _, ok := config["region"]; !ok {
			panic("'region' must be specified in config for SQS provider type")
		}
		if _, ok := config["awsAccountId"]; !ok {
			panic("'awsAccountId' must be specified in config for SQS provider type")
		}
		invVar, err := strconv.Atoi(config["awsAccountId"])
		if err != nil {
			panic("'awsAccountId' cannot be converted to an integer")
		}
		provider := AmazonMessageService{
			config:    config,
			wg:        wg,
			region:    config["region"],
			accountId: invVar,
		}
		return &provider
	}
	return nil
}

type ActiveSubscription struct {
	Id          uuid.UUID
	Shutdown    chan struct{} //should always initialize with buffer of size 1. Not every provider will listen to this channel (like SQS)
	TargetName  string
	IsTopic     bool
	MessageChan chan *publicmodels.QueueMessage
}
