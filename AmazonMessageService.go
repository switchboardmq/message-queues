package messagequeues

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/sns"
	snsTypes "github.com/aws/aws-sdk-go-v2/service/sns/types"
	"github.com/aws/aws-sdk-go-v2/service/sqs"
	"github.com/aws/aws-sdk-go-v2/service/sqs/types"
	"github.com/google/uuid"
	"gitlab.com/switchboardmq/messagequeues/clients"
	"gitlab.com/switchboardmq/publicmodels"
	"go.uber.org/multierr"
	"log"
	"reflect"
	"sync"
	"time"
)

type AmazonMessageService struct {
	config              map[string]string
	wg                  *sync.WaitGroup
	sqsClient           clients.ISqsClient
	snsClient           clients.ISnsClient
	region              string
	accountId           int
	activeSubscriptions map[string]*ActiveSubscription
}

func (p *AmazonMessageService) Initialize() error {
	cfg, err := config.LoadDefaultConfig(context.Background(),
		config.WithRegion(p.region),
	)
	if err != nil {
		return err
	}
	p.snsClient = clients.NewSnsClient(cfg)
	p.sqsClient = clients.NewSqsClient(cfg)
	p.activeSubscriptions = make(map[string]*ActiveSubscription)
	return nil
}

func (p *AmazonMessageService) registerSubscription(subscription *ActiveSubscription) error {
	if val, ok := p.activeSubscriptions[subscription.Id.String()]; ok {
		if val != nil {
			return errors.New("this target already has an active subscription")
		}
	}
	p.activeSubscriptions[subscription.TargetName] = subscription
	return nil

}

func (p *AmazonMessageService) unregisterSubscription(subscription ActiveSubscription) error {
	if val, ok := p.activeSubscriptions[subscription.Id.String()]; ok {
		if val == nil {
			return errors.New(fmt.Sprintf("subscription with target name '%s' already unregistered.", subscription.TargetName))
		}
		p.activeSubscriptions[subscription.TargetName] = nil
		return nil
	}
	return errors.New(fmt.Sprintf("subscription with target name '%s' was never registered.", subscription.TargetName))

}

func (p *AmazonMessageService) isSubscriptionRegistered(id uuid.UUID) bool {
	if val, ok := p.activeSubscriptions[id.String()]; ok {
		return val != nil
	}
	return false
}

func (p *AmazonMessageService) SubscribeToQueue(queueName string, bufferSize int) (<-chan *publicmodels.QueueMessage, error) {
	subscription := ActiveSubscription{
		TargetName: queueName,
		IsTopic:    false,
	}
	err := p.registerSubscription(&subscription)
	if err != nil {
		return nil, err
	}
	messageChan := make(chan *publicmodels.QueueMessage, bufferSize)
	go p.subscribeToQueue(queueName, messageChan, subscription, func() {})

	return messageChan, nil
}

func (p *AmazonMessageService) UnsubscribeFromQueue(queueName string) error {
	if val, ok := p.activeSubscriptions[queueName]; ok {
		return p.unregisterSubscription(*val)
	}
	return nil
}

func (p *AmazonMessageService) UnsubscribeFromTopic(topicName string) error {
	return p.UnsubscribeFromQueue(topicName)
}

// SubscribeToTopic subscribes to the given topic name. You may also pass a filter that allows you to subscribe to
// only certain messages based on the payload
func (p *AmazonMessageService) SubscribeToTopic(topicName string, filter map[string][]any, bufferSize int) (<-chan *publicmodels.QueueMessage, error) {
	subscription := ActiveSubscription{
		TargetName: topicName,
		IsTopic:    true,
	}
	messageChan := make(chan *publicmodels.QueueMessage, bufferSize)
	err := p.registerSubscription(&subscription)
	if err != nil {
		return nil, err
	}
	//sqs does not allow direct subscriptions to topics. Need to create a temp queue that subscribes to the topic
	tempQueueName, subscriptionArn, err := p.createTempQueueWithTopicSubscription(topicName, filter)
	if err != nil {
		return nil, err
	}

	//this callback will only be called when the subscription is unregistered.
	cb := func() {
		//will reach this point on shutdown
		err = p.deleteTempQueueAndSubscription(tempQueueName, subscriptionArn)
		if err != nil {
			log.Println("[WARN] error deleting temp queue and subscription: ", err)
		}
	}
	go p.subscribeToQueue(tempQueueName, messageChan, subscription, cb)

	return messageChan, nil
}

func (p *AmazonMessageService) PublishSingleMessageToQueue(queueName string, message any) error {
	return p.sendMessageToSwitchboardQueue(queueName, message)
}

func (p *AmazonMessageService) PublishBulkMessagesToQueue(queueName string, messages any) error {
	return p.sendMessagesToSwitchboardQueue(queueName, messages)
}

func (p *AmazonMessageService) PublishSingleMessageToTopic(topicName string, message any) error {
	return p.sendMessageToSwitchboardTopic(topicName, message)
}

func (p *AmazonMessageService) PublishBulkMessagesToTopic(topicName string, messages any) error {
	return p.sendMessagesToSwitchboardTopic(topicName, messages)
}

func (p *AmazonMessageService) subscribeToQueue(queueName string, messageChan chan *publicmodels.QueueMessage, subscription ActiveSubscription, cb func()) {
	p.wg.Add(1)
	defer p.wg.Done()
	defer log.Printf("[INFO] unsubscribed from %s", queueName)
	for {
		//shutdown if the subscription was unregistered
		if !p.isSubscriptionRegistered(subscription.Id) {
			cb()
			close(messageChan)
			break
		}

		reqInput := sqs.ReceiveMessageInput{
			QueueUrl:            &queueName,
			MaxNumberOfMessages: 10,
			VisibilityTimeout:   240,
			WaitTimeSeconds:     10,
		}
		messageOut, err := p.sqsClient.ReceiveMessage(context.Background(), &reqInput)
		if err != nil {
			log.Println("[ERROR] SQS read message error: ", err)
			<-time.After(5 * time.Second)
			continue
		}
		if messageOut == nil {
			continue
		}
		for _, msg := range messageOut.Messages {
			out := publicmodels.QueueMessage{
				MessageId: *msg.MessageId,
				Body:      *msg.Body,
				Ack: func() {
					p.cleanupMessage(msg, queueName)
				},
			}
			messageChan <- &out
		}
	}
}

func (p *AmazonMessageService) sendMessagesToSwitchboardQueue(queueName string, messages any) error {
	var entries []types.SendMessageBatchRequestEntry

	if reflect.ValueOf(messages).Kind() != reflect.Slice {
		return errors.New("messages must be of type 'slice'")
	}
	var messageList []any
	c := reflect.ValueOf(messages).Len()
	for i := 0; i < c; i++ {
		messageList[i] = reflect.ValueOf(messages).Index(i).Interface()
	}
	idsAndIndexes := make(map[string]int)
	for i, message := range messageList {
		messageBytes, err := json.Marshal(message)
		if err != nil {
			log.Printf("[ERROR]: could not marshal message %s", message)
			continue
		}
		id, _ := uuid.NewRandom()
		idsAndIndexes[id.String()] = i
		entry := types.SendMessageBatchRequestEntry{
			Id:          aws.String(id.String()),
			MessageBody: aws.String(string(messageBytes)),
		}
		entries = append(entries, entry)
	}
	input := sqs.SendMessageBatchInput{
		Entries:  entries,
		QueueUrl: &queueName,
	}
	result, err := p.sqsClient.SendMessageBatch(context.Background(), &input)
	var errorList error
	if err != nil {
		log.Print(err)

		for i, m := range messageList {
			//try sending one at a time to see if it fixes the problem
			//this solves the issue when the bulk payload is too large
			err = p.sendMessageToSwitchboardQueue(queueName, m)
			if err != nil {
				errorList = multierr.Append(errorList, errors.New(fmt.Sprintf("could not publish message with Reason: %s, Index: %v", err, i)))
			}
		}
		return errorList
	}

	for _, r := range result.Failed {
		msg := "no reason provided"
		if r.Message != nil {
			msg = *r.Message
		}
		errorList = multierr.Append(errorList, errors.New(fmt.Sprintf("could not publish message with Reason: %s, Index: %v", msg, idsAndIndexes[*r.Id])))
	}
	return errorList
}

func (p *AmazonMessageService) sendMessageToSwitchboardQueue(queueName string, message any) error {
	messageBytes, err := json.Marshal(message)
	if err != nil {
		log.Printf("[ERROR] could not marshal message for message %s", message)
		return err
	}
	input := sqs.SendMessageInput{
		MessageBody: aws.String(string(messageBytes)),
		QueueUrl:    &queueName,
	}
	_, err = p.sqsClient.SendMessage(context.Background(), &input)
	if err != nil {
		return err
	}
	return nil
}

func (p *AmazonMessageService) sendMessagesToSwitchboardTopic(topicName string, messages any) error {
	var entries []snsTypes.PublishBatchRequestEntry

	if reflect.ValueOf(messages).Kind() != reflect.Slice {
		return errors.New("messages must be of type 'slice'")
	}
	var messageList []any
	c := reflect.ValueOf(messages).Len()
	for i := 0; i < c; i++ {
		messageList[i] = reflect.ValueOf(messages).Index(i).Interface()
	}
	idsAndIndexes := make(map[string]int)
	for i, message := range messageList {
		messageBytes, err := json.Marshal(message)
		if err != nil {
			log.Printf("[ERROR]: could not marshal message %s", message)
			continue
		}
		id, _ := uuid.NewRandom()
		idsAndIndexes[id.String()] = i
		entry := snsTypes.PublishBatchRequestEntry{
			Id:      aws.String(id.String()),
			Message: aws.String(string(messageBytes)),
		}
		entries = append(entries, entry)
	}
	input := sns.PublishBatchInput{
		PublishBatchRequestEntries: entries,
		TopicArn:                   aws.String(fmt.Sprintf("arn:aws:sns:%s:%v:%s", p.region, p.accountId, topicName)),
	}
	result, err := p.snsClient.PublishBatch(context.Background(), &input)
	var errorList error
	if err != nil {
		log.Print(err)

		for i, m := range messageList {
			//try sending one at a time to see if it fixes the problem
			//this solves the issue when the bulk payload is too large
			err = p.sendMessageToSwitchboardTopic(topicName, m)
			if err != nil {
				errorList = multierr.Append(errorList, errors.New(fmt.Sprintf("could not publish message with Reason: %s, Index: %v", err, i)))
			}
		}
		return errorList
	}

	for _, r := range result.Failed {
		msg := "no reason provided"
		if r.Message != nil {
			msg = *r.Message
		}
		errorList = multierr.Append(errorList, errors.New(fmt.Sprintf("could not publish message with Reason: %s, Index: %v", msg, idsAndIndexes[*r.Id])))
	}
	return errorList
}

func (p *AmazonMessageService) sendMessageToSwitchboardTopic(topicName string, message any) error {
	messageBytes, err := json.Marshal(message)
	if err != nil {
		log.Printf("[ERROR] could not marshal message for message %s", message)
		return err
	}
	input := sns.PublishInput{
		Message:  aws.String(string(messageBytes)),
		TopicArn: aws.String(fmt.Sprintf("arn:aws:sns:%s:%v:%s", p.region, p.accountId, topicName)),
	}
	_, err = p.snsClient.Publish(context.Background(), &input)
	if err != nil {
		return err
	}
	return nil
}

func (p *AmazonMessageService) cleanupMessage(msg types.Message, queueName string) {

	_, err := p.sqsClient.DeleteMessage(
		context.Background(),
		&sqs.DeleteMessageInput{
			QueueUrl:      &queueName,
			ReceiptHandle: msg.ReceiptHandle,
		})
	if err != nil {
		log.Println("[ERROR] SQS could not delete message. Trying again later: ", err)
	}
}
