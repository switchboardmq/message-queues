package messagequeues

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/service/sns"
	"github.com/aws/aws-sdk-go-v2/service/sqs"
	"github.com/aws/aws-sdk-go-v2/service/sqs/types"
	"github.com/google/uuid"
	"log"
)

type AwsPolicy struct {
	Version    string      `json:"Version"`
	Id         string      `json:"Id"`
	Statements []Statement `json:"Statement"`
}

type Statement struct {
	Sid       string    `json:"Sid"`
	Effect    string    `json:"Effect"`
	Principal Principal `json:"Principal"`
	Action    []string  `json:"Action"`
	Resource  string    `json:"Resource"`
	Condition Condition `json:"Condition"`
}

type Principal struct {
	Service string `json:"Service"`
}

type Condition struct {
	ArnEquals ArnEquals `json:"ArnEquals"`
}

type ArnEquals struct {
	AwsSourceArn string `json:"aws:SourceArn"`
}

func (a AwsPolicy) String() string {
	b, _ := json.Marshal(a)
	return string(b)
}

func createNewTempSqsQueuePolicy(config map[string]string, queueName string) AwsPolicy {
	id, _ := uuid.NewRandom()
	id2, _ := uuid.NewRandom()
	return AwsPolicy{
		Version: "2012-10-17",
		Id:      id.String(),
		Statements: []Statement{{
			Sid:    id2.String(),
			Effect: "Allow",
			Principal: Principal{
				Service: "sns.amazonaws.com",
			},
			Action:   []string{"sqs:SendMessage", "sqs:SendMessageBatch"},
			Resource: fmt.Sprintf("arn:aws:sqs:%s:%s:%s", config["awsRegion"], config["awsAccountId"], queueName),
			Condition: Condition{
				ArnEquals: ArnEquals{
					AwsSourceArn: fmt.Sprintf("arn:aws:sns:%s:%s:%s", config["awsRegion"], config["awsAccountId"], config["subscriptionChangeTopicName"]),
				},
			},
		}},
	}
}

// returns the temp queue url
func (p *AmazonMessageService) createTempQueueWithTopicSubscription(topicName string, filter map[string][]any) (string, string, error) {
	tempQueueID, _ := uuid.NewRandom()
	tempQueueName := fmt.Sprintf("workflowChanges__%s", tempQueueID.String())
	createQueueInput := sqs.CreateQueueInput{
		QueueName: &tempQueueName,
		Attributes: map[string]string{
			"MessageRetentionPeriod": "60",
			"Policy":                 createNewTempSqsQueuePolicy(p.config, tempQueueName).String(),
		},
		Tags: nil,
	}
	log.Println("Creating temp queue...")
	//create a temp queue that will subscribe to the change topic
	newQueueOutput, err := p.sqsClient.CreateQueue(context.Background(), &createQueueInput)
	if err != nil {
		log.Println("error creating queue: ", err)
		return "", "", err
	}
	queueAttrInput := sqs.GetQueueAttributesInput{
		QueueUrl:       newQueueOutput.QueueUrl,
		AttributeNames: []types.QueueAttributeName{types.QueueAttributeNameQueueArn},
	}
	queueAttrOutput, err := p.sqsClient.GetQueueAttributes(context.Background(), &queueAttrInput)
	if err != nil {
		log.Println("could not get queue attributes with temp queue url. Error: ", err)
		return "", "", err
	}

	subscriptionAttributes := map[string]string{
		"RawMessageDelivery": "true",
	}
	if len(filter) > 0 {
		filterBytes, err := json.Marshal(filter)
		if err == nil {
			subscriptionAttributes["FilterPolicy"] = string(filterBytes)
		}

	}
	subscribeInput := sns.SubscribeInput{
		Protocol:              aws.String("sqs"),
		TopicArn:              aws.String(fmt.Sprintf("arn:aws:sns:%s:%s:%s", p.config["awsRegion"], p.config["awsAccountId"], topicName)),
		Attributes:            subscriptionAttributes,
		Endpoint:              aws.String(queueAttrOutput.Attributes[fmt.Sprintf("%s", types.QueueAttributeNameQueueArn)]),
		ReturnSubscriptionArn: true,
	}
	log.Println("Subscribing to topic...")
	//subscribe to event subscription changes topic...
	subscription, err := p.snsClient.Subscribe(context.Background(), &subscribeInput)
	if err != nil {
		log.Println("error subscribing to queue: ", err)
		newErr := p.deleteQueue(tempQueueName)
		if newErr != nil {
			log.Println("error deleting queue after subscription to topic error: ", newErr)
		}
		return "", "", err
	}
	return tempQueueName, *subscription.SubscriptionArn, nil
}

func (p *AmazonMessageService) deleteTempQueueAndSubscription(tempQueueName string, arn string) error {
	err := p.deleteQueue(tempQueueName)
	if err != nil {
		return err
	}
	log.Println("[INFO] temp queue deleted")
	return p.unsubscribeFromTopic(arn)
}

func (p *AmazonMessageService) deleteQueue(name string) error {
	deleteQueueInput := sqs.DeleteQueueInput{
		QueueUrl: &name,
	}
	_, err := p.sqsClient.DeleteQueue(context.Background(), &deleteQueueInput)
	return err
}

func (p *AmazonMessageService) unsubscribeFromTopic(arn string) error {
	unsubscribeInput := sns.UnsubscribeInput{
		SubscriptionArn: &arn,
	}
	_, err := p.snsClient.Unsubscribe(context.Background(), &unsubscribeInput)
	if err != nil {
		log.Println("[INFO] temp queue topic subscription deleted")
	}
	return err
}
