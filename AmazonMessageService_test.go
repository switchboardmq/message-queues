package messagequeues

import (
	"context"
	"errors"
	"github.com/aws/aws-sdk-go-v2/service/sns"
	"github.com/aws/aws-sdk-go-v2/service/sqs/types"
	"github.com/google/uuid"
	"gitlab.com/switchboardmq/messagequeues/clients"
	"gitlab.com/switchboardmq/messagequeues/clients/testClients"
	"gitlab.com/switchboardmq/publicmodels"
	"reflect"
	"sync"
	"testing"
)

func TestAmazonMessageService_Initialize(t *testing.T) {
	type fields struct {
		config              map[string]string
		wg                  *sync.WaitGroup
		sqsClient           clients.ISqsClient
		snsClient           clients.ISnsClient
		region              string
		accountId           int
		activeSubscriptions map[string]*ActiveSubscription
	}
	tests := []struct {
		name    string
		fields  fields
		wantErr bool
	}{
		{
			name: "should run",
			fields: fields{
				config:              nil,
				wg:                  nil,
				sqsClient:           nil,
				snsClient:           nil,
				region:              "",
				accountId:           0,
				activeSubscriptions: nil,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &AmazonMessageService{
				config:              tt.fields.config,
				wg:                  tt.fields.wg,
				sqsClient:           tt.fields.sqsClient,
				snsClient:           tt.fields.snsClient,
				region:              tt.fields.region,
				accountId:           tt.fields.accountId,
				activeSubscriptions: tt.fields.activeSubscriptions,
			}
			if err := p.Initialize(); (err != nil) != tt.wantErr {
				t.Errorf("Initialize() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestAmazonMessageService_PublishBulkMessagesToQueue(t *testing.T) {
	type fields struct {
		config              map[string]string
		wg                  *sync.WaitGroup
		sqsClient           clients.ISqsClient
		snsClient           clients.ISnsClient
		region              string
		accountId           int
		activeSubscriptions map[string]*ActiveSubscription
	}
	type args struct {
		queueName string
		messages  any
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &AmazonMessageService{
				config:              tt.fields.config,
				wg:                  tt.fields.wg,
				sqsClient:           tt.fields.sqsClient,
				snsClient:           tt.fields.snsClient,
				region:              tt.fields.region,
				accountId:           tt.fields.accountId,
				activeSubscriptions: tt.fields.activeSubscriptions,
			}
			if err := p.PublishBulkMessagesToQueue(tt.args.queueName, tt.args.messages); (err != nil) != tt.wantErr {
				t.Errorf("PublishBulkMessagesToQueue() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestAmazonMessageService_PublishBulkMessagesToTopic(t *testing.T) {
	type fields struct {
		config              map[string]string
		wg                  *sync.WaitGroup
		sqsClient           clients.ISqsClient
		snsClient           clients.ISnsClient
		region              string
		accountId           int
		activeSubscriptions map[string]*ActiveSubscription
	}
	type args struct {
		topicName string
		messages  any
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &AmazonMessageService{
				config:              tt.fields.config,
				wg:                  tt.fields.wg,
				sqsClient:           tt.fields.sqsClient,
				snsClient:           tt.fields.snsClient,
				region:              tt.fields.region,
				accountId:           tt.fields.accountId,
				activeSubscriptions: tt.fields.activeSubscriptions,
			}
			if err := p.PublishBulkMessagesToTopic(tt.args.topicName, tt.args.messages); (err != nil) != tt.wantErr {
				t.Errorf("PublishBulkMessagesToTopic() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestAmazonMessageService_PublishSingleMessageToQueue(t *testing.T) {
	type fields struct {
		config              map[string]string
		wg                  *sync.WaitGroup
		sqsClient           clients.ISqsClient
		snsClient           clients.ISnsClient
		region              string
		accountId           int
		activeSubscriptions map[string]*ActiveSubscription
	}
	type args struct {
		queueName string
		message   any
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &AmazonMessageService{
				config:              tt.fields.config,
				wg:                  tt.fields.wg,
				sqsClient:           tt.fields.sqsClient,
				snsClient:           tt.fields.snsClient,
				region:              tt.fields.region,
				accountId:           tt.fields.accountId,
				activeSubscriptions: tt.fields.activeSubscriptions,
			}
			if err := p.PublishSingleMessageToQueue(tt.args.queueName, tt.args.message); (err != nil) != tt.wantErr {
				t.Errorf("PublishSingleMessageToQueue() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestAmazonMessageService_PublishSingleMessageToTopic(t *testing.T) {
	type fields struct {
		config              map[string]string
		wg                  *sync.WaitGroup
		sqsClient           clients.ISqsClient
		snsClient           clients.ISnsClient
		region              string
		accountId           int
		activeSubscriptions map[string]*ActiveSubscription
	}
	type args struct {
		topicName string
		message   any
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &AmazonMessageService{
				config:              tt.fields.config,
				wg:                  tt.fields.wg,
				sqsClient:           tt.fields.sqsClient,
				snsClient:           tt.fields.snsClient,
				region:              tt.fields.region,
				accountId:           tt.fields.accountId,
				activeSubscriptions: tt.fields.activeSubscriptions,
			}
			if err := p.PublishSingleMessageToTopic(tt.args.topicName, tt.args.message); (err != nil) != tt.wantErr {
				t.Errorf("PublishSingleMessageToTopic() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestAmazonMessageService_SubscribeToQueue(t *testing.T) {
	type fields struct {
		config              map[string]string
		wg                  *sync.WaitGroup
		sqsClient           clients.ISqsClient
		snsClient           clients.ISnsClient
		region              string
		accountId           int
		activeSubscriptions map[string]*ActiveSubscription
	}
	type args struct {
		queueName  string
		bufferSize int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    <-chan *publicmodels.QueueMessage
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &AmazonMessageService{
				config:              tt.fields.config,
				wg:                  tt.fields.wg,
				sqsClient:           tt.fields.sqsClient,
				snsClient:           tt.fields.snsClient,
				region:              tt.fields.region,
				accountId:           tt.fields.accountId,
				activeSubscriptions: tt.fields.activeSubscriptions,
			}
			got, err := p.SubscribeToQueue(tt.args.queueName, tt.args.bufferSize)
			if (err != nil) != tt.wantErr {
				t.Errorf("SubscribeToQueue() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("SubscribeToQueue() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAmazonMessageService_SubscribeToTopic(t *testing.T) {
	type fields struct {
		config              map[string]string
		wg                  *sync.WaitGroup
		sqsClient           clients.ISqsClient
		snsClient           clients.ISnsClient
		region              string
		accountId           int
		activeSubscriptions map[string]*ActiveSubscription
	}
	type args struct {
		topicName  string
		bufferSize int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    <-chan *publicmodels.QueueMessage
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &AmazonMessageService{
				config:              tt.fields.config,
				wg:                  tt.fields.wg,
				sqsClient:           tt.fields.sqsClient,
				snsClient:           tt.fields.snsClient,
				region:              tt.fields.region,
				accountId:           tt.fields.accountId,
				activeSubscriptions: tt.fields.activeSubscriptions,
			}
			got, err := p.SubscribeToTopic(tt.args.topicName, make(map[string][]any), tt.args.bufferSize)
			if (err != nil) != tt.wantErr {
				t.Errorf("SubscribeToTopic() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("SubscribeToTopic() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAmazonMessageService_UnsubscribeFromQueue(t *testing.T) {
	type fields struct {
		config              map[string]string
		wg                  *sync.WaitGroup
		sqsClient           clients.ISqsClient
		snsClient           clients.ISnsClient
		region              string
		accountId           int
		activeSubscriptions map[string]*ActiveSubscription
	}
	type args struct {
		queueName string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &AmazonMessageService{
				config:              tt.fields.config,
				wg:                  tt.fields.wg,
				sqsClient:           tt.fields.sqsClient,
				snsClient:           tt.fields.snsClient,
				region:              tt.fields.region,
				accountId:           tt.fields.accountId,
				activeSubscriptions: tt.fields.activeSubscriptions,
			}
			if err := p.UnsubscribeFromQueue(tt.args.queueName); (err != nil) != tt.wantErr {
				t.Errorf("UnsubscribeFromQueue() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestAmazonMessageService_UnsubscribeFromTopic(t *testing.T) {
	type fields struct {
		config              map[string]string
		wg                  *sync.WaitGroup
		sqsClient           clients.ISqsClient
		snsClient           clients.ISnsClient
		region              string
		accountId           int
		activeSubscriptions map[string]*ActiveSubscription
	}
	type args struct {
		topicName string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &AmazonMessageService{
				config:              tt.fields.config,
				wg:                  tt.fields.wg,
				sqsClient:           tt.fields.sqsClient,
				snsClient:           tt.fields.snsClient,
				region:              tt.fields.region,
				accountId:           tt.fields.accountId,
				activeSubscriptions: tt.fields.activeSubscriptions,
			}
			if err := p.UnsubscribeFromTopic(tt.args.topicName); (err != nil) != tt.wantErr {
				t.Errorf("UnsubscribeFromTopic() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestAmazonMessageService_cleanupMessage(t *testing.T) {
	type fields struct {
		config              map[string]string
		wg                  *sync.WaitGroup
		sqsClient           clients.ISqsClient
		snsClient           clients.ISnsClient
		region              string
		accountId           int
		activeSubscriptions map[string]*ActiveSubscription
	}
	type args struct {
		msg       types.Message
		queueName string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &AmazonMessageService{
				config:              tt.fields.config,
				wg:                  tt.fields.wg,
				sqsClient:           tt.fields.sqsClient,
				snsClient:           tt.fields.snsClient,
				region:              tt.fields.region,
				accountId:           tt.fields.accountId,
				activeSubscriptions: tt.fields.activeSubscriptions,
			}
			p.cleanupMessage(tt.args.msg, tt.args.queueName)
		})
	}
}

func TestAmazonMessageService_registerSubscription(t *testing.T) {
	type fields struct {
		config              map[string]string
		wg                  *sync.WaitGroup
		sqsClient           clients.ISqsClient
		snsClient           clients.ISnsClient
		region              string
		accountId           int
		activeSubscriptions map[string]*ActiveSubscription
	}
	type args struct {
		subscription *ActiveSubscription
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &AmazonMessageService{
				config:              tt.fields.config,
				wg:                  tt.fields.wg,
				sqsClient:           tt.fields.sqsClient,
				snsClient:           tt.fields.snsClient,
				region:              tt.fields.region,
				accountId:           tt.fields.accountId,
				activeSubscriptions: tt.fields.activeSubscriptions,
			}
			p.registerSubscription(tt.args.subscription)
		})
	}
}

func TestAmazonMessageService_sendMessageToSwitchboardQueue(t *testing.T) {
	type fields struct {
		config              map[string]string
		wg                  *sync.WaitGroup
		sqsClient           clients.ISqsClient
		snsClient           clients.ISnsClient
		region              string
		accountId           int
		activeSubscriptions map[string]*ActiveSubscription
	}
	type args struct {
		queueName string
		message   any
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &AmazonMessageService{
				config:              tt.fields.config,
				wg:                  tt.fields.wg,
				sqsClient:           tt.fields.sqsClient,
				snsClient:           tt.fields.snsClient,
				region:              tt.fields.region,
				accountId:           tt.fields.accountId,
				activeSubscriptions: tt.fields.activeSubscriptions,
			}
			if err := p.sendMessageToSwitchboardQueue(tt.args.queueName, tt.args.message); (err != nil) != tt.wantErr {
				t.Errorf("sendMessageToSwitchboardQueue() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestAmazonMessageService_sendMessageToSwitchboardTopic(t *testing.T) {
	type fields struct {
		config              map[string]string
		wg                  *sync.WaitGroup
		sqsClient           clients.ISqsClient
		snsClient           clients.ISnsClient
		region              string
		accountId           int
		activeSubscriptions map[string]*ActiveSubscription
	}
	snsTestClientWithFailure := testClients.TestSnsClient{
		PublishFunc: func(ctx context.Context, input *sns.PublishInput) (*sns.PublishOutput, error) {
			return nil, errors.New("something happened")
		},
	}
	snsTestClientWithSuccess := testClients.TestSnsClient{
		PublishFunc: func(ctx context.Context, input *sns.PublishInput) (*sns.PublishOutput, error) {
			return &sns.PublishOutput{}, nil
		},
	}
	type args struct {
		topicName string
		message   any
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "basic failure",
			fields: fields{
				config:              nil,
				wg:                  nil,
				sqsClient:           nil,
				snsClient:           &snsTestClientWithFailure,
				region:              "us-east-1",
				accountId:           123,
				activeSubscriptions: nil,
			},
			args: args{
				topicName: "test",
				message: publicmodels.EventMessagePayload{
					WorkflowId:  uuid.UUID{},
					MessageId:   uuid.UUID{},
					EventId:     uuid.UUID{},
					ContentType: "json",
					Payload:     "data",
				},
			},
			wantErr: true,
		},
		{
			name: "basic success",
			fields: fields{
				config:              nil,
				wg:                  nil,
				sqsClient:           nil,
				snsClient:           &snsTestClientWithSuccess,
				region:              "us-east-1",
				accountId:           123,
				activeSubscriptions: nil,
			},
			args: args{
				topicName: "test",
				message: publicmodels.EventMessagePayload{
					WorkflowId:  uuid.UUID{},
					MessageId:   uuid.UUID{},
					EventId:     uuid.UUID{},
					ContentType: "json",
					Payload:     "data",
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &AmazonMessageService{
				config:              tt.fields.config,
				wg:                  tt.fields.wg,
				sqsClient:           tt.fields.sqsClient,
				snsClient:           tt.fields.snsClient,
				region:              tt.fields.region,
				accountId:           tt.fields.accountId,
				activeSubscriptions: tt.fields.activeSubscriptions,
			}
			if err := p.sendMessageToSwitchboardTopic(tt.args.topicName, tt.args.message); (err != nil) != tt.wantErr {
				t.Errorf("sendMessageToSwitchboardTopic() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestAmazonMessageService_sendMessagesToSwitchboardQueue(t *testing.T) {
	type fields struct {
		config              map[string]string
		wg                  *sync.WaitGroup
		sqsClient           clients.ISqsClient
		snsClient           clients.ISnsClient
		region              string
		accountId           int
		activeSubscriptions map[string]*ActiveSubscription
	}
	type args struct {
		queueName string
		messages  any
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &AmazonMessageService{
				config:              tt.fields.config,
				wg:                  tt.fields.wg,
				sqsClient:           tt.fields.sqsClient,
				snsClient:           tt.fields.snsClient,
				region:              tt.fields.region,
				accountId:           tt.fields.accountId,
				activeSubscriptions: tt.fields.activeSubscriptions,
			}
			if err := p.sendMessagesToSwitchboardQueue(tt.args.queueName, tt.args.messages); (err != nil) != tt.wantErr {
				t.Errorf("sendMessagesToSwitchboardQueue() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestAmazonMessageService_sendMessagesToSwitchboardTopic(t *testing.T) {
	type fields struct {
		config              map[string]string
		wg                  *sync.WaitGroup
		sqsClient           clients.ISqsClient
		snsClient           clients.ISnsClient
		region              string
		accountId           int
		activeSubscriptions map[string]*ActiveSubscription
	}
	type args struct {
		topicName string
		messages  any
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &AmazonMessageService{
				config:              tt.fields.config,
				wg:                  tt.fields.wg,
				sqsClient:           tt.fields.sqsClient,
				snsClient:           tt.fields.snsClient,
				region:              tt.fields.region,
				accountId:           tt.fields.accountId,
				activeSubscriptions: tt.fields.activeSubscriptions,
			}
			if err := p.sendMessagesToSwitchboardTopic(tt.args.topicName, tt.args.messages); (err != nil) != tt.wantErr {
				t.Errorf("sendMessagesToSwitchboardTopic() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestAmazonMessageService_subscribeToQueue(t *testing.T) {
	type fields struct {
		config              map[string]string
		wg                  *sync.WaitGroup
		sqsClient           clients.ISqsClient
		snsClient           clients.ISnsClient
		region              string
		accountId           int
		activeSubscriptions map[string]*ActiveSubscription
	}
	type args struct {
		queueName    string
		messageChan  chan *publicmodels.QueueMessage
		subscription ActiveSubscription
		cb           func()
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &AmazonMessageService{
				config:              tt.fields.config,
				wg:                  tt.fields.wg,
				sqsClient:           tt.fields.sqsClient,
				snsClient:           tt.fields.snsClient,
				region:              tt.fields.region,
				accountId:           tt.fields.accountId,
				activeSubscriptions: tt.fields.activeSubscriptions,
			}
			p.subscribeToQueue(tt.args.queueName, tt.args.messageChan, tt.args.subscription, tt.args.cb)
		})
	}
}

func TestAmazonMessageService_unregisterSubscription(t *testing.T) {
	type fields struct {
		config              map[string]string
		wg                  *sync.WaitGroup
		sqsClient           clients.ISqsClient
		snsClient           clients.ISnsClient
		region              string
		accountId           int
		activeSubscriptions map[string]*ActiveSubscription
	}
	activeSubs := make(map[string]*ActiveSubscription)

	testOne := &ActiveSubscription{
		Id:          uuid.New(),
		Shutdown:    nil,
		TargetName:  "test",
		IsTopic:     false,
		MessageChan: nil,
	}
	activeSubs[testOne.Id.String()] = testOne

	testTwo := &ActiveSubscription{
		Id:          uuid.New(),
		TargetName:  "testTwo",
		IsTopic:     false,
		MessageChan: nil,
	}
	activeSubs[testTwo.Id.String()] = nil
	type args struct {
		subscription ActiveSubscription
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "should remove subscription from list",
			fields: fields{
				region:              "",
				accountId:           0,
				activeSubscriptions: activeSubs,
			},
			args:    args{subscription: *activeSubs[testOne.Id.String()]},
			wantErr: false,
		},
		{
			name: "should fail if subscription is not active",
			fields: fields{
				region:              "",
				accountId:           0,
				activeSubscriptions: make(map[string]*ActiveSubscription),
			},
			args:    args{subscription: *testTwo},
			wantErr: true,
		},
		{
			name: "should fail if subscription is was already unsubscribed",
			fields: fields{
				region:              "",
				accountId:           0,
				activeSubscriptions: activeSubs,
			},
			args:    args{subscription: *testTwo},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &AmazonMessageService{
				config:              tt.fields.config,
				wg:                  tt.fields.wg,
				sqsClient:           tt.fields.sqsClient,
				snsClient:           tt.fields.snsClient,
				region:              tt.fields.region,
				accountId:           tt.fields.accountId,
				activeSubscriptions: tt.fields.activeSubscriptions,
			}
			if err := p.unregisterSubscription(tt.args.subscription); (err != nil) != tt.wantErr {
				t.Errorf("unregisterSubscription() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
